# Changelog

## 0.7.9

- In the interface: add a checkbox in the choice of the resource to validate to allow the user to
choice header case-sensitivity or not
- Change urls which referred to the private gitlab instance of OpendataFrance to point toward 
the new urls of gitlab public instances

## 0.7.8

- Edit .gitlab-ci.yml file to run pipelines with self-hosted runner named validata-table

## 0.7.7

- Restore gitlab-ci.yaml file content as in v0.7.5

## 0.7.6

- Try to fix failed pipeline launched with a new release tag at building new Docker image stage 

## 0.7.5

- Fix bug with internal server error related to url redirections
- Fix opening-hours-py, pydantic and typing-extensions packages versions in requirements.txt
- Upgrade requirements (validata-core 0.9.7)

## 0.7.4

- Upgrade requirements (validata-core 0.9.6)

## 0.7.3

- Fix build docker image: Upgrade opening-hours-py to version 0.5.6 in requirements

## 0.7.2

- Upgrade requirements (validata-core 0.9.4)

## 0.7.1

- Upgrade requirements (validata-core 0.9.3)

## 0.7.0

- Solve confusion when schema is unvalid
- Add flash-error for duplicate labels in headers of data source
- Allows to ignore header-case sensitivity
- Upgrade requirements (validata-core 0.9.2)
- Update footer
- Update table schema url
- Add config file
- Update gitlab ci

## 0.6.12

- Add required dependency (`0.8.6`)

## 0.6.11

- Add required dependency (`shapely`)

## 0.6.10

- Upgrade requirements (validata-core `0.8.5`)

## 0.6.9

- Fix publishing issue

## 0.6.8

- Fix tab browsing (re-enable JS)

## 0.6.7

- Follow [lighthouse](https://developers.google.com/web/tools/lighthouse/) recommandations to improve home page
- Improve homepage loading speed, defering update in background job
- Improve schema pages loading speed

## 0.6.6

- Remove trailing spaces in report cells
- Add info on max uploaded file size

## 0.6.5

- Fix bug relative to task-errors

## 0.6.4

- Update REAME
- Improve task error display
- Add FAQ link in header template that links to FAQ section of docs/README.md
- Add minimal doc stuff (in docs/ folder) containing a FAQ

## 0.6.3

- Better display custom checks configuration errors
- Upgrade requirements (validata-core `0.8.4`)

## 0.6.2

- Container image is now based on Debian 11 (`bullseye`) slim: newer, lighter
- Container image contains less unused stuff (better .dockerignore file)
- Upgrade requirements (validata-core `0.8.3`)

## 0.6.1

- Update requirements (validata-core `0.8.1`)

## 0.6.0

- Update requirements (validata-core `0.8.0`):
  - `phone-number-value` custom-check
  - `opening-hours-value` custom-check
  - fixes in `nomenclature-actes-value` custom-check

## 0.5.8

- Update requirements (validata-core `0.7.6`) to fix https://git.opendatafrance.net/validata/validata-ui/-/issues/104

## 0.5.7

- Update requirements (validata-core `0.7.5`) to fix https://git.opendatafrance.net/validata/validata-ui/-/issues/102

## 0.5.6

- Update requirements (validata-core `0.7.4` with frictionless `4.10.6`)

## 0.5.5

- Update requirements (validata-core `0.7.3`: encoding exception)

## 0.5.4

- Fix access to config variables in jinja2 templates (restoring matomo and sentry tracker)

## 0.5.3

- Upgrade validata-core requirement to `0.7.2` (fix UTF-8 BOM issue)

## 0.5.2

- Upgrade requirements (fixup)

## 0.5.1

- Upgrade requirements (validata-core 0.7.1) to support .xls files
- Freeze frictionless-py version (4.2.1)

## 0.5.0

- Merge `towards_frictionless_4` branch into master
- Update requirements (validata-core 0.7.0)

## 0.5.0a4 (`towards_frictionless_4` branch)

- fix flake8 issues
- update requirements (validata-core 0.7.0a7)
- rephrase presentation in footer

## 0.5.0a3 (`towards_frictionless_4` branch)

- update requirements (frictionless 4.1.0)

## 0.5.0a2 (`towards_frictionless_4` branch)

- update requirements (validata-core 0.7.0a2)
- add explicit error message when a tabular data as url can't be retrieved (404)
- improve error message ([issue](https://git.opendatafrance.net/validata/validata-ui/-/issues/82)]

## 0.5.0a1 (`towards_frictionless_4` branch)

- update requirements
- adapt to changes in validation report

## 0.4.1

- add explicit error message when a tabular data as an URL can't be retrieved (404)
- improve error message ([issue](https://git.opendatafrance.net/validata/validata-ui/-/issues/82)]

## 0.4.0

- constraint frictionless version
- update README
- fix packaging by adding `validata.css` and `validata_popover.js`

## 0.4.0a22

- display only tableschema (using new `schema_type` property)
- move most of dependency from requirements.in to setup.py
- use packaged versions of validata-core
- filter tableschemas
- restore Chromium headless PDF renderer as default PDF renderer

## 0.4.0a21

- fix source rows count display
- display missing required header as structure error
- restore filename display in report page
- move recommendations block before structure errors
- upgrade validata-core version to 0.6.0a9

## 0.4.0a20

- upgrade validata-core version to 0.6.0a8
- improve no-cache settings
- handle non-table errors

## 0.4.0a19

- upgrade validata-core version to 0.6.0a7

## 0.4.0a18

- add cache using [requests-cache](https://pypi.org/project/requests-cache/)

## 0.4.0a17

- update to validata-core 0.6.0a6
- use new validata_core functions: `is_body_error` and `is_structure_error`

## 0.4.0a16

- remove unused `.prettierrc`
- configure dev-containers
- fix JSON data POST for browserless.io

## 0.4.0a15

- increase gunicorn timeout

## 0.4.0a14

- add "Content-type: application/json" to POST headers to browserless.io service
- let pupppeter wait for requests to finish
- improve logging with JSON data
- use flaskenv
- configure linter and formatter

## 0.4.0a13

- add loggging around browserless.io service call

## 0.4.0a12

- stop using `chromium` to generate PDF report
- optional: use browserless.io to render PDF report

## 0.4.0a11

- Upgrade to validata-core 0.6.0a4 (that fix encoding issues)

## 0.4.0a10

- Fix chromium install in `Dockerfile` (fonts install)
- Add `--disable-dev-shm-usage` parameter to chromium headless command line

## 0.4.0a9

- Add dbus package install in `Dockerfile`
- Add `--disable-gpu` to chromium args

## 0.4.0a8

- Fix error sorting
- Remove unused code

## 0.4.0a7

- Fix PDF generation code flow

## 0.4.0a6

- Add explicit error message on startup if `CONFIG` env variable is not set
- Fix PDF generation temporary file early removal
- Use black

## 0.4.0a5

- Fix `Recommandations` title display
- Restore PDF report link

## 0.4.0a4

- Fix Dockerfile chromium version

## 0.4.0a3

- Move validata resource code to validata-core
- clean code
- upgrade to last validata-core release
- frictionless dependency is only used for TableSchema definition

## 0.4.0a2

- Replace `homepage_config.json` file by `config.yaml` that contains homepage schemas and catalog settings but also header and footer links

## 0.4.0a1

- Fix badge display

## 0.4.0a0

- Migrate to new library [frictionless-py](https://github.com/frictionlessdata/frictionless-py) that replaces `goodtables-py`.

## 0.3.10

- Sentry integration

## 0.3.9

- Fix dependencies problem

## 0.3.8

- Fix error handling
- Improve error display on home page

## 0.3.7

- Code refactoring
- Upgrade to validata-core 0.5.6

## 0.3.6

- Upgrade to validata-core 0.5.5

## 0.3.5

- Fix crash on invalid table schema

## 0.3.4

- Improve duplicate-header errors display
- Upgrade to validata-core 0.5.4

## 0.3.3

- Adjustments in validation report display
- Upgrade to validata-core 0.5.3

## 0.3.2

- Upgrade to validata-core 0.5.2

## 0.3.1

- Upgrade to validata-core 0.5.1

## 0.3.0

- Repair tabular file structure prior to look for value errors

## 0.2.22

- Fix crash on home page if non existant repo

## 0.2.21

- Fix https://git.opendatafrance.net/validata/validata-ui/issues/64 (commonmark to HTML conversion)

## 0.2.20

- home page : allow HTML description for section

## 0.2.18

- better catalog error handling
- fix schema order in home page selects

## 0.2.15

- Catalog description on home page

## 0.2.11

- Sticky footer
- Fix crash on uploaded file validation

## 0.2.7

Technical changes:

- Replace Flask-Matomo by a client-side JavaScript function
- Remove configuration variable `MATOMO_AUTH_TOKEN`

## 0.2.6

Technical changes:

- Fix PDF report generation

## 0.2.4

New features for users:

- Render Markdown links table header, in the table that shows validation errors
- Ease clicking on links in popups, in the table that shows validation errors

## 0.2.2

Non-breaking changes:

- Don't fail if error loading catalog

## 0.2.1

Non-breaking changes:

- display version number in footer

## 0.2.0

New features for users:

- validate a tabular file (e.g. CSV) against a schema given by URL
- allow configuring homepage sections and blocks with a JSON config file (see `HOMEPAGE_CONFIG` environment variable in `.env`)
- allow validating against previous versions of schemas

Breaking changes:

- URLs changed because of schema version support

Non-breaking changes:

- UI now requests `validata-api` service to do the validation, and does not depend on `validata-core` anymore
- a Dockerfile has been added
- a Continuous Integration pipeline has been added
  - the Docker image is rebuilt for each release
  - the Python package is uploaded to [PyPI](https://pypi.org/) for each release

## 0.1.0

Non-breaking changes:

- Display badge in report page (optional, see `BADGE_CONFIG_URL` in `.env.example`)
