Veuillez renseigner au maximum les informations suivantes pour nous aider à comprendre et à résoudre le problème que vous avez rencontré :

## Résumé

Décrivez le problème rencontré de façon concise

## Étapes pour reproduire le problème

Quelles étapes pouvons-nous suivre pour reproduire le même problème

- URL de la page concernée
  - exemple : https://go.validata.fr/table-schema?input=url&schema_name=scdl.deliberations&schema_ref=v2.1.3&url=https://gitlab.com/opendatafrance/scdl/deliberations/-/raw/master/examples/DeliberationsCindoc.csv

## Comportement actuel

Décrivez ce qui se passe

## Comportement attendu

Décrivez ce à quoi vous vous attendiez

## Fichier concerné

- nom du schéma utilisé (exemples : prénoms, délibérations, etc.)
- URL du fichier validé :
- sinon mettez le fichier en pièce jointe

## Capture d'écran

Ajoutez une capture d'écran ou copiez-collez la dans le texte

## Suggestions

Si vous avez une idée de comment résoudre le problème
