FROM python:3.9-slim-bullseye
LABEL maintainer="admin-validata@jailbreak.paris"

# Env variables that configure Python to run in a container:
# do not keep dependencies downloaded by "pip install"
ENV PIP_NO_CACHE_DIR=1
# do not write "*.pyc" files
ENV PYTHONDONTWRITEBYTECODE=1
# do not buffer input/output operations, displaying prints and log messages immediately
ENV PYTHONUNBUFFERED=1

EXPOSE 5000

RUN apt-get update && \
  apt-get install --yes chromium xfonts-100dpi xfonts-75dpi xfonts-base

RUN pip install gunicorn==20.1.0

ARG uid=1000
ARG gid=1000

RUN groupadd -g ${gid} validata && \
  useradd -u ${uid} -g ${gid} --create-home --shell /bin/bash validata

# Cf https://bugs.chromium.org/p/chromium/issues/detail?id=638180 and https://blog.ineat-conseil.fr/2018/08/executer-karma-avec-chromeheadless-dans-un-conteneur-docker/
USER validata

WORKDIR /home/validata/

COPY --chown=validata:validata requirements.txt .
RUN pip install --user --requirement requirements.txt --no-warn-script-location

COPY --chown=validata:validata . .
RUN pip install --user --editable .

ENV CONFIG_FILE=config.example.yaml

# Number of workers is defined by `WEB_CONCURRENCY` env variable.
# Cf https://docs.gunicorn.org/en/stable/settings.html#workers
CMD gunicorn --bind 0.0.0.0:5000 --timeout 60 validata_ui:app
